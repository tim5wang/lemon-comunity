function [ y ] = weighted_NMI(clu1,clu2, value_nmi)
%WNMI: Calculate the value of the weighted NMI.
%   clu1: cluster 1
%   clu2: cluster 2

n1 = max(clu1);
n2 = max(clu2);

if nargin == 2
    nmi = nmi_fun(clu1,clu2);
elseif (nargin == 3)
    nmi = value_nmi;
end

y = nmi * exp(-abs(n1-n2)/n2);

end

