function [ name, type,network,real_label ] = load_LFR( lambda )
%Load LFR netwroks.
%   name: The name of the input network. 
%   type: The structure of the input network.

global network real_label sign;

switch lambda
    case '0.00'
        %======================= LFR1000-0.00 ======================
        network = load('LFR1000\0.00.txt');
        real_label = load('LFR1000\0.00.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.05'
        %======================= LFR1000-0.05 ======================
        network = load('LFR1000\0.05.txt');
        real_label = load('LFR1000\0.05.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.10'
        %======================= LFR1000-0.10 ======================
        network = load('LFR1000\0.10.txt');
        real_label = load('LFR1000\0.10.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.15'
        %======================= LFR1000-0.15 ======================
        network = load('LFR1000\0.15.txt');
        real_label = load('LFR1000\0.15.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.20'
        %======================= LFR1000-0.20 ======================
        network = load('LFR1000\0.20.txt');
        real_label = load('LFR1000\0.20.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.25'
        %======================= LFR1000-0.25 ======================
        network = load('LFR1000\0.25.txt');
        real_label = load('LFR1000\0.25.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.30'
        %======================= LFR1000-0.30 ======================
        network = load('LFR1000\0.30.txt');
        real_label = load('LFR1000\0.30.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.35'
        %======================= LFR1000-0.35 ======================
        network = load('LFR1000\0.35.txt');
        real_label = load('LFR1000\0.35.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.40'
        %======================= LFR1000-0.40 ======================
        network = load('LFR1000\0.40.txt');
        real_label = load('LFR1000\0.40.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.45'
        %======================= LFR1000-0.45 ======================
        network = load('LFR1000\0.45.txt');
        real_label = load('LFR1000\0.45.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.50'
        %======================= LFR1000-0.50 ======================
        network = load('LFR1000\0.50.txt');
        real_label = load('LFR1000\0.50.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.55'
        %======================= LFR1000-0.55 ======================
        network = load('LFR1000\0.55.txt');
        real_label = load('LFR1000\0.55.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.60'
        %======================= LFR1000-0.60 ======================
        network = load('LFR1000\0.60.txt');
        real_label = load('LFR1000\0.60.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.65'
        %======================= LFR1000-0.65 ======================
        network = load('LFR1000\0.65.txt');
        real_label = load('LFR1000\0.65.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.70'
        %======================= LFR1000-0.70 ======================
        network = load('LFR1000\0.70.txt');
        real_label = load('LFR1000\0.70.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.75'
        %======================= LFR1000-0.75 ======================
        network = load('LFR1000\0.75.txt');
        real_label = load('LFR1000\0.75.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
    case '0.80'
        %======================= LFR1000-0.80 ======================
        network = load('LFR1000\0.80.txt');
        real_label = load('LFR1000\0.80.clu');
        real_label = decode(real_label);
        name = 'LFR1000';
        sign = 'unsigned';
        type = 'known';
end

end

