function [ y ] = mutation_based_similarity( ind )
%MUTATION_BASED_ON_SIMILARITY: This function generates a new individual 
% through processing the selected individual based on mutation operator.
%   ind: a selected individual from the population.

global sim_mat;

data = ind.net_data;
temp_nodes = find_bounday_nodes(data);

boundary_nodes = [temp_nodes.overlapping_nodes, temp_nodes.nonoverlapping_nodes];

n = length(boundary_nodes);
if n > 0
    select_node_index = ceil(rand*n);
    select_node = boundary_nodes(select_node_index);
    sim_vet = sim_mat(select_node, :);
    neighbors = find(sim_vet > 0);
    n_neighbors = length(neighbors);
    total_sim_value = sum(sim_vet);
    sim_vet_without0 = sim_vet(neighbors);

    % roulette wheel selection based on the similarity matrix.
    temp_value = sim_vet_without0 ./ total_sim_value;
    accumulation_sim = zeros(1,n_neighbors);
    for i = 1:n_neighbors
        accumulation_sim(i) = sum(temp_value(1:i));
    end
    pick = rand;
    for i = 1:n_neighbors
        if (pick - accumulation_sim(i)) < 0
            tab = i;
            break
        end
    end

    data.label(select_node) = data.label(neighbors(tab));
    y = data;
else
    y = [];
end

end