The algorithm GMOEA-net we wrote is based on MOEA/D, which is provided at https://sites.google.com/view/moead/resources. You can run GMOEA-net by entering demo(runtimes, [lambda, [P_neg, P_pos]]) in the command window.

Parameters:
	runtimes        : The number of runs.
	lambda[Optional]: The mixing parameter �� in the computer-generated networks. 
	P_neg [Optional]: The controlling parameter P- in the signed LFR networks.
	P_pos [Optional]: The controlling parameter P+ in the signed LFR networks.
	
Since we provided the LFR network in the package, you could enter demo(10, '0.10') to test the LFR network. That means, the LFR network with the mixing parameter 0.10 will be tested 10 times. 