function subp=init_weights(popsize, niche, objDim)
% INIT_WEIGHTS: The function initialize a pupulation of subproblems structure
% with the generated decomposition weight and the neighbourhood
% relationship.
    subp=[];
    for i=0:popsize
        if objDim==2
            p=struct('weight',[],'neighbour',[], 'net_data', []);
            weight=zeros(2,1);
            weight(1)=i/popsize;
            weight(2)=(popsize-i)/popsize;
            p.weight=weight;
            subp=[subp p];
        elseif objDim==3
        end
    end

    %Set up the neighbourhood.
    leng=length(subp);
    distanceMatrix=zeros(leng, leng);
    for i=1:leng
        for j=i+1:leng
            A=subp(i).weight;B=subp(j).weight;
            distanceMatrix(i,j)=(A-B)'*(A-B);       
            distanceMatrix(j,i)=distanceMatrix(i,j);
        end
        [~,sindex]=sort(distanceMatrix(i,:));
        subp(i).neighbour=sindex(1:niche)';
    end
    
end