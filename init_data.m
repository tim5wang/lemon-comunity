function ind = init_data(prob, n, network)  
%INIT_DATA: The function generates n new points randomly from the mop problem given.
% prob    : mod
% n       : The size of population.
% network : Adjacent matrix of the test network.

if (nargin==1)
    n=1;
end

indiv = struct('label',[], 'adjacent_nodes',[],'objective',[], 'estimation', []);
adjacent_nodes = struct('pos_adjacent',[],'neg_adjacent',[]);

indiv.label = 1:prob.pd;     % initialize label.
for i = 1:prob.pd
    pos_neighbor = find(network(i,:) > 0);
    neg_neighbor = find(network(i,:) < 0);
    adjacent_nodes.pos_adjacent{i} = pos_neighbor;
    adjacent_nodes.neg_adjacent{i} = neg_neighbor; 
end
indiv.adjacent_nodes = adjacent_nodes;
ind = repmat(indiv, 1, n);
 
end
