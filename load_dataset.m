function [ name, type] = load_dataset( )
%Load real-world networks.

global network real_label sign;
%======================= Signed networks ======================
% %%------------- illustrate1 -----------------
% network = load('signed_networks\sample1.txt');
% real_label = load('real_label\sample_label.txt');
% real_label = decode(real_label);
% name = 'illustrate1 network';
% sign = 'signed';
% type = 'known';
%%------------- illustrate2 -----------------
% network = load('signed_networks\sample2.txt');
% real_label = load('real_label\sample_label.txt');
% real_label = decode(real_label);
% name = 'illustrate2 network';
% sign = 'signed';
% type = 'known';
%%%-------------- SPP network ----------------
% network = load('signed_networks\SPP.txt');
% real_label = load('real_label\SPP_label.txt');
% real_label = decode(real_label);
% name = 'SPP network';
% sign = 'signed';
% type = 'known';
%%%-------------- GGS network ----------------
% network = load('signed_networks\GGS.txt');
% real_label = load('real_label\GGS_label.txt');
% real_label = decode(real_label);
% name = 'GGS network';
% sign = 'signed';
% type = 'known';
%%%--------------- WikiData ------------------
% network = load('signed_networks\WikiData.txt');
% name = 'Wikipedia network';
% sign = 'signed';
% type = 'unknown';

%%%-------------- testing example ----------------
% network = load('signed_networks\test1.txt');
% real_label = load('real_label\test_real1.txt');
% real_label = decode(real_label);
% name = 'test network1';
% sign = 'signed';
% type = 'known';
%%%-------------- testing example ----------------
% network = load('signed_networks\test2.txt');
% real_label = load('real_label\test_real2.txt');
% real_label = decode(real_label);
% name = 'test network2';
% sign = 'signed';
% type = 'known';

%========================== Unsigned netowrks ==========================
% %%------------- karate network -----------------
% network = load('unsigned_networks\karate.txt');
% real_label = load('real_label\karate_label.txt');
% real_label = decode(real_label);
% name = 'karate network';
% sign = 'unsigned';
% type = 'known';
%%------------- dolphin network -----------------
% network = load('unsigned_networks\dolphin.txt');
% real_label = load('real_label\dolphin_label.txt');
% real_label = decode(real_label);
% name = 'dolphin network';
% sign = 'unsigned';
% type = 'known';
% %%------------- polbook network -----------------
network = load('unsigned_networks\polbook.txt');
real_label = load('real_label\polbook_label.txt');
real_label = decode(real_label);
name = 'polbook network';
sign = 'unsigned';
type = 'known';
% % %%------------- football network -----------------
% network = load('unsigned_networks\football.txt');
% real_label = load('real_label\football_label.txt');
% real_label = decode(real_label);
% name = 'football network';
% sign = 'unsigned';
% type = 'known';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % %%------------- SFI network -----------------
% network = load('unsigned_networks\SFI.txt');
% real_label = [];
% name = 'SFI network';
% sign = 'unsigned';
% type = 'unknown';
% % %%------------- netscience network -----------------
% network = load('unsigned_networks\netscience.txt');
% real_label = [];
% name = 'netscience network';
% sign = 'unsigned';
% type = 'unknown';

end

