function [v, x] = evaluate( prob, x, network )
%EVALUATE: The function evaluates an individual structure of a vector point with
%the given multiobjective problem.

%   Detailed explanation goes here
%   prob: is the multiobjective problem.
%   x: is a vector point, or a individual structure.
%   v: is the result objectives evaluated by the mop.
%   x: if x is an individual structure, then x's objective field is modified
%   with the evaluated value and pass back.

%   TODO, need to refine it to operate on a vector of points.
    if isstruct(x)
        x.label = decode(x.label);
        v = prob.func(x, network);       
        x.objective = v;
        % printf('The dimension of objective fun: %d * %d',size(v,1),size(v,2));
    else
        label = decode(x);
        v = prob.func(label, network);
    end
end