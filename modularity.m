function [ y ] = modularity(x, network )
%MODULARITY: Calculate the modularity Q.

% L: the total links in the network
L = sum(sum(network))/2;

if isstruct(x)       
    label = x.label;
else                
    label = x;
end
 
Q = 0;
n = max(label); % 社区数目
for i = 1:n
    index = find(label == i);  % 找到同一社区里的节点
    S = network(index,index); % 社区里节点发出的边或到达这个节点的边
    li = sum(sum(S))/2; % 计算这个社区里节点里面的边的总数  link
    di = 0; % 
    for j = 1:length(index)
        di = di + sum(network(index(j),:));
    end
    Q = Q + (li - ((di)^2)/(4*L));
end
y = Q/L;
end

