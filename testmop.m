function mop = testmop(name, dimension, type)
%TESTMOP: Get test multi-objective problems from a given name.
%   name     : The name of the input network.
%   dimension: The dimensions of the decision variable or the number of
%              nodes in the input network.
%   type     : The structure type of the input network(i.e. known or unknown).

global sign;

if (nargin == 2)
    type = 'known';
end

mop = struct('name',[],'od',[],'pd',[],'type',[],'func',[]);
% Construct problem-specific struct.
% name : the name of function;
% od   : the number of functions;
% pd   : the number of nodes;
% func : the specific function.
switch lower(sign)
    case 'unsigned'
        mop=unsigned_fun(name, mop, dimension, type);
    case 'signed'
        mop=signed_fun(name, mop,dimension, type);
    otherwise 
        error('Undefined test problem name');                
end 
end

% function generator of unsigned networks.
function p=unsigned_fun(name, p, dim, structure_type)
 p.name = name;
 p.pd   = dim;
 p.od   = 2;
 p.type = structure_type; 
 p.func = @evaluate;
 p.funQ = false;
    % evaluation function for unsigned networks.
    function y = evaluate(x, network)
        y = zeros(p.od,1);
        % x是label标签
        % 计算比值关联值，也就是给迭代到当前步时的社区划分算社区划分好坏
        y(1) = ratio_association(x, network); % 社区内的连接数和
        y(2) = negative_ratio_cut(x, network); % 跨社区的连接数和
    end
end
% function generator of signed networks.
function p=signed_fun(name, p, dim, structure_type)
 p.name = name;
 p.pd = dim;
 p.od = 2;
 p.type = structure_type;
 p.func = @evaluate;
 p.funQ = true;
    % evaluation function for signed networks
    function y = evaluate(x, network)
        y = zeros(p.od,1);
        y(1) = signed_modularity(x, network);
        y(2) = edge_density(x, network);
        p.funQ = false;
    end
end
 