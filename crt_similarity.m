function sim = crt_similarity(x)
% CRT_SIMILARITY: Create the similarity matrix for a network.
% 网络的相似矩阵
if isfield(x, 'adjacent_nodes')
    x = x.adjacent_nodes;
end

    n = length(x.pos_adjacent);
    pos_s = zeros(n,n);
    neg_s = zeros(n,n);
    
    pos_degree = zeros(1,n);
    neg_degree = zeros(1,n);
    for i = 1:n
        for j = 1:length(x.pos_adjacent{i})
            neighbor = x.pos_adjacent{i}(j); 
            % intersect是求交集
            pos_com_elements = intersect(x.pos_adjacent{i},x.pos_adjacent{neighbor}); 
            neg_com_elements = intersect(x.neg_adjacent{i},x.neg_adjacent{neighbor});
            % The number of common neighbors for two nodes which have positive connection.
            % 具有正连接的两个节点的公共邻居数
            com_num = length(pos_com_elements) + length(neg_com_elements);  
            pos_s(i,neighbor) = com_num + 1;
        end
        for k = 1:length(x.neg_adjacent{i})
            neighbor = x.neg_adjacent{i}(k);
            com_elements = intersect(x.pos_adjacent{i},x.pos_adjacent{neighbor});
            % The number of common neighbors for two nodes which have negative connection.
            com_num = length(com_elements);                                 
            neg_s(i,neighbor) = com_num;
        end
        pos_degree(i) = length(x.pos_adjacent{i});
        neg_degree(i) = length(x.neg_adjacent{i});
    end
    
    sim = pos_s + neg_s;                % create similarity matrix
    degree = pos_degree + neg_degree;   
    
    for i = 1:n
        for j = (i+1):n
            if (degree(i) == 0) || (degree(j) == 0)
                sim(i,j) = 0;
                sim(j,i) = 0;
            else
                sim(i,j) = sim(i,j)/sqrt(degree(i)*degree(j));
                sim(j,i) = sim(i,j);
            end
        end
    end
    
end
