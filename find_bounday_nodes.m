function [ boundary_nodes ] = find_bounday_nodes( x )
%FIND_BOUNDRY_NODES: This function is devised to find boundary nodes which 
% have connections with multi-communities.
%   x            : the label of an individual or a struct
%   bounday_nodes: the set of boundary nodes.

global network;
n = length(network);

if isstruct(x)
    label = x.label;
    pos_neighbors = x.adjacent_nodes.pos_adjacent;
    boundary_nodes = struct('nonoverlapping_nodes',[],'overlapping_nodes',[]);
    for i = 1:n
        is_common_community = (label(pos_neighbors{i}) ~= label(i));
        neighbor_number = sum(is_common_community);
        if (neighbor_number > 0) && (length(pos_neighbors{i}) ~= 2)
            boundary_nodes.nonoverlapping_nodes = [boundary_nodes.nonoverlapping_nodes i];
        elseif (neighbor_number > 0) && (length(pos_neighbors{i}) == 2)
            boundary_nodes.overlapping_nodes = [boundary_nodes.overlapping_nodes, i];
        end
    end
else
    label = x;
    boundary_nodes = struct('nonoverlapping_nodes',[],'overlapping_nodes',[]);
    for i = 1:n
        pos_neighbors = (network(i,:) > 0);
        is_common_community = (label(pos_neighbors) ~= label(i));
        neighbor_number = sum(is_common_community);
        if (neighbor_number > 0) && (length(pos_neighbors) ~= 2)
            boundary_nodes.nonoverlapping_nodes = [boundary_nodes.nonoverlapping_nodes i];
        elseif (neighbor_number > 0) && (length(pos_neighbors) == 2)
            boundary_nodes.overlapping_nodes = [boundary_nodes.overlapping_nodes, i];
        end
    end
end

end

