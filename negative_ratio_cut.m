function [ y ] = negative_ratio_cut( x, network )
% NEGATIVE_RATIO_CUT: Calculate the value of negative ratio cut.
%   x: The label of an individual or a struct.
% 这个函数算的是社区外的连接数量总和，直观上看越小越好
if isstruct(x)
    label = x.label;
else
    label = x;
end

k = max(label);
inter_degree = 0;       
for i = 1:k
    degree = 0;
    index = find(label == i);
    ni = length(index);
    for j = 1:ni
        neighbors = (network(index(j),:) > 0); % 这个社区里某个节点相关的所有其他节点
        degree = degree + sum(label(neighbors) ~= i); % 如果发现这个节点不是他们社区的就加上他的度
    end
    inter_degree = inter_degree + degree/ni;
end

y = - inter_degree;

end

