function [ y ] = ratio_association( x, network )
% RATIO_ASSOCIATION: Calculate the value of ratio association.
%   x: The label of an individual or a struct.
% 计算每个社区内部的出入度和，并对每个社区的度和进行求和，直观上可以感觉到，这个值越大代表划分的越好
if isstruct(x)
    label = x.label;
else
    label = x;
end

inner_degree = 0;       
k = max(label);         % the number of clusters.
for i = 1:k
    index = find(label == i);
    S = network(index,index);
    ni = length(index);
    degree = sum(sum(S))/ni;
    inner_degree = inner_degree + degree;  % 出入度和
end

y = inner_degree;
end

