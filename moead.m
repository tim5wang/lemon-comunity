function pareto = moead( mop, network, sign, varargin)
%MOEAD runs moea/d algorithms for the given mop.
%   Detailed explanation goes here
%   The mop must to be minimizing. 
%   The parameters of the algorithms can be set through varargin. including
%   popsize: The subproblem's size.
%   niche: the neighboursize, must less then the popsize.
%   iteration: the total iteration of the moead algorithms before finish.
%   method: the decomposition method, the value can be 'ws' or 'ts'.

    starttime = clock;
    % global variable definition.
    global params idealpoint objDim parDim itrCounter sim_mat;
    
    % set the parameters of the proposed algorithm.
    paramIn = varargin;
    tic
    [objDim, parDim, idealpoint, params, population, sim_mat] = init(mop, network, sign, paramIn);
    fprintf('The initialization phase took %fs.\n', toc);
    
    itrCounter=1;
    while ~terminate(itrCounter)
        tic;
        population = evolve(population, mop, network);
        fprintf(sprintf('iteration %u finished, time used: %u \n', itrCounter, toc));
        itrCounter=itrCounter+1;
    end
    % display the result.
    pareto=[population.net_data];
%   pp=[pareto.objective];
    fprintf(sprintf('total time used %u \n', etime(clock, starttime)));
    
%   corr_index = correlation_index(pp);
%   fprintf('The correlation index of two objective function is %f\n', corr_index);
end

%%
function [objDim, parDim, idealp, params, population, sim_mat]=init(mop, network, sign, propertyArgIn)
% Set up the initial setting for MOEA/D.
    objDim=mop.od;
    parDim=mop.pd;    
    idealp=ones(objDim,1)*(-inf);   % the reference point.
    
    % the default values for the parameters.
    params.popsize=100;
    params.niche=20;
    params.iteration=100;
    params.dmethod='ts';
    params.pc = 0.8;  
    params.pm = 0.2;
    
    % handle the parameters, mainly about the popsize
    while length(propertyArgIn)>=2
        prop = propertyArgIn{1};
        val=propertyArgIn{2};
        propertyArgIn=propertyArgIn(3:end);

        switch prop
            case 'popsize'
                params.popsize=val;
            case 'niche'
                params.niche=val;
            case 'iteration'
                params.iteration=val;
            case 'method'
                params.dmethod=val;
            otherwise
                warning('moea doesnot support the given parameters name');
        end
    end
    
    % ��ʼ��Ȩ��
    population = init_weights(params.popsize, params.niche, objDim);
    params.popsize = length(population);  
  
    % initial the subproblem's initital state.
    inds = init_data(mop, params.popsize, network);
   
    % the process of preprocessing.
    sim_mat = crt_similarity(inds(1));       % create the similarity matrix.
    for j = 1:params.popsize              
        inds(j) = preprocessing(inds(j), sim_mat, sign);
    end
    
    V = [];
    for j = 1:params.popsize
        [temp1, temp2] = evaluate(mop, inds(j), network);
        V = [V, temp1];
        population(j).net_data = temp2;
    end
    idealp = max(idealp, max(V,[],2));
    
end

%%
function population = evolve(population, mop, network)
    global idealpoint;
    for i=1:length(population)
        %new point generation using genetic operations, and evaluate it.
        ind = genetic_op(population, i);   
        num = length(ind);
        obj = zeros(2,num);
        for j = 1:num
            [obj(:,j),ind(j)] = evaluate(mop, ind(j), network);    
        end
        %update the idealpoint.
        if ~isempty(obj)
            idealpoint = max(idealpoint, max(obj,[],2));
        end
        
        %update the neighbours.
        neighbourindex = population(i).neighbour;
        for k = 1:num                       
            population(neighbourindex)=update(population(neighbourindex),ind(k), idealpoint);
        end
        %clear ind obj neighbourindex neighbours;
    end
 
end

%%
function subp =update(subp, ind, idealpoint)
    global params
    % we choose Tchebycheff approach
    newobj=subobjective([subp.weight], ind.objective, idealpoint, params.dmethod);
    oops = [subp.net_data]; 
    oldobj=subobjective([subp.weight], [oops.objective], idealpoint, params.dmethod );
    
    C = newobj < oldobj;
    [subp(C).net_data]= deal(ind);  
%   clear C newobj oops oldobj;
end

%%
function y =terminate(itrcounter)
    global params;
    y = itrcounter>params.iteration;
end
