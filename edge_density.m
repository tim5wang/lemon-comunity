function [ y ] = edge_density( x, network )
%EDGE_DENSITY: Calculate the value of edge density.
%   x: The label of an individual or a struct of the new_data;
%   y: The value of the edge density. 
% 计算边密度
edge_density = 0;
if isstruct(x)   % if x is a structure variable.
    label = x.label;
    n = length(label);
    nnodes = x.adjacent_nodes;
    k = max(label);     
    for i = 1:k
        index = find(label == i);  % 找某一类（社群）的节点 
        ni = length(index); % 节点个数
        S = network(index,index); % 取这些节点相关的边
        pos_ind = sum(sum(S > 0)); % 计算正关系（不考虑正负就是1）条数
        if (ni~=1) % 不是孤立的
            posin_density = pos_ind/(ni*(ni-1)); % 入方向的权重
        elseif (ni==1) && (sum(network(index,:)) ~= 0) % 是孤立的，并且有边
            posin_density = 0;     
        else % 孤立的且没有变连向他
            posin_density = 1;     
        end
        neg_outd = 0;
        for j = 1:ni
            neg_neighbor = nnodes.neg_adjacent{index(j)};
            tab = label(neg_neighbor);
            neg_outd = neg_outd + sum(tab~=i);
        end
        if n == ni
            negout_density = 0;
        else
            negout_density = neg_outd/(ni*(n-ni));  % 出方向的权重
        end
        edge_density = edge_density + posin_density + negout_density;
    end
    edge_density = edge_density / k;
else             % or x is the label of an individual.
    label = x;
    n = length(label);
    k = max(label);
    for i = 1:k
        index = find(label == i);
        ni = length(index);
        S = network(index,index);
        pos_ind = sum(sum(S > 0));
        if (ni~=1)
            posin_density = pos_ind/(ni*(ni-1));
        elseif (ni==1) && (sum(network(index,:)) ~= 0)
            posin_density = 0;     
        else
            posin_density = 1;   
        end
        neg_outd = 0;
        for j = 1:ni
            neg_pos = (network(index(j),:) < 0);
            tab = label(neg_pos);
            neg_outd = neg_outd + sum(tab~=i);
        end
        if n == ni
            negout_density = 0;
        else
            negout_density = neg_outd/(ni*(n-ni));
        end
        edge_density = edge_density + posin_density + negout_density;
    end
    edge_density = edge_density / k;
end

y = edge_density;

end

