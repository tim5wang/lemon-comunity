% function demo(runtimes, lambda, P_neg, P_pos)
% DEMO: You are supposed to input arguments to run the GMOEA-net. You can
% use it by input demo(1, '0.00') in the command window to test the LFR 
% network. 
%       runtimes: The parameter controls the number of GMOEA-net runs.
%       lambda  : This parameter is only used in the computer-generated
%                 network datasets.
%       P_neg   : This parameter is only used in SLFR netwroks. 
%       P_pos   : This parameter is only used in SLFR networks. 


global network sign real_label;
% network    : The adjacent matrix of test networks;
% sign       : The sign of test networks(signed or unsigned). 
% real_label : The real label of test networks(may not have for some
% test networks).
lambda = '0.00'
runtimes = 1
% Load networks.
% if nargin == 1          % test the real-world networks.
%     [name, type] = load_dataset();
% elseif nargin == 2      % test the unsigned computed-generated networks.
    [name, type] = load_LFR(lambda); 
% elseif nargin == 3      % test the signed computed-generated networks.
%     [name, type] = load_SLFR(lambda, P_neg, P_pos);
% else
%     error('Wrong input, at least one parameter.');
% end
mop = testmop(name, length(network), type);
%% mop似乎是minimum optimization 的缩写，记为最小值优化问题
%% mop是一个六元组，包含了问题名称，函数个数，节点个数，目标函数，
%% 其中目标函数根据一个全局变量确定是按照有符号还是无符号的进行
for it = 1:runtimes
    %每次运行的结果         被迭代的元组，网络，有无符号，子问题大小，领域大小，迭代次数，分解方法                    
    pareto{it} = moead( mop, network, sign, 'popsize', 99, 'niche', 20, 'iteration', 100, 'method', 'te'); 
end

%%
% calculate the value of unsigned Q or signed Q; true/1代表是有符号的
if ~mop.funQ
   for i = 1:length(pareto)
       current_pareto = pareto{i};
       popsize = length(current_pareto);
       temp_SQ = zeros(1, popsize);
       for j = 1:popsize
           % 计算模量Q，什么是模量Q？模块性？衡量社区好坏的指标？看论文！！搜modularity，得知这是第二个目标函数
           % 这个是针对有符号的图
           temp_SQ(j) = modularity(current_pareto(j).label, network);
       end
       SQ(i) = max(temp_SQ);
   end
else
   for i = 1:length(pareto)
       pp = [pareto{i}.objective];
       max_sq = max(pp(1,:));
       SQ(i) = max_sq;
   end
end

fprintf('The maximum modularity is %f.\n', max(SQ));
 
% calculate the value of nmi and wnmi.
 if strcmpi(mop.type, 'known')
     nmi = struct('value',[],'label',[],'community_number',[]);
     wnmi = struct('value',[],'label',[],'community_number',[]);
     for i = 1:length(pareto)
         current_pareto = pareto{i};
         popsize = length(current_pareto);
         temp_nmi = zeros(1,popsize);
         temp_wnmi = zeros(1,popsize);
         
         for j = 1:popsize        % we can use parfor here to accelerate the running speed.
             temp_nmi(j) = nmi_fun(current_pareto(j).label, real_label);
             temp_wnmi(j) = weighted_NMI(current_pareto(j).label, real_label, temp_nmi(j));
         end
        
         [nmi.value(i), index1] = max(temp_nmi);
         nmi.label(i,:) = current_pareto(index1).label;
         nmi.community_number(i) = max(nmi.label(i,:));
         
         [wnmi.value(i), index2] = max(temp_wnmi);
         wnmi.label(i,:) = current_pareto(index2).label;
         wnmi.community_number(i) = max(wnmi.label(i,:));
     end
 else
     nmi = [];
     wnmi = [];
 end
 
 if strcmpi(mop.type, 'known')
     fprintf('The maximum NMI is %f.\n', max(nmi.value));
     fprintf('The maximum WNMI is %f.\n', max(wnmi.value));
      
% output_to_file(mop, SQ, nmi, wnmi, lambda, P_neg, P_pos, name);

end