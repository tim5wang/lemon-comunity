function [ cluster_assignment ] = decode(g)
%Convert the label 'g' to a contiguous value starting at one.
%   g : the label of an individual.
% 返回 1行 1000列的数组，值代表节点的label
N = length(g);
cluster_assignment = zeros(1, N);
current_cluster = 1;

for i = 1:N
    if (cluster_assignment(i) == 0)
        index = (g == g(i));
        cluster_assignment(index) = current_cluster;
        current_cluster = current_cluster + 1;
    end
end
end

