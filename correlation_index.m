function [ y ] = correlation_index( pop )
%CORRELATION_INDEX: To calculate the correlation index.
% 计算相关系数,这个函数似乎没用到
if isstruct(pop)
    ind = [pop.net_data];
    obj = [ind.objevtive];
else
    obj = pop;
end

m = mean(obj,2);
cova = (obj(1,:)-m(1))*(obj(2,:)-m(2))';  % calculate the covariance 协方差
variance1 = sum((obj(1,:)-m(1)).^2);
variance2 = sum((obj(2,:)-m(2)).^2);
y = cova/(sqrt(variance1)*sqrt(variance2));
end

